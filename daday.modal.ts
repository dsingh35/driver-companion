export interface DaDay {
    clockIn?: {time: Date | number; location: {lat: number; long: number}; };
    fStop?: boolean;
    fAttempt?: boolean;
    lAttempt?: boolean;
    rts?: boolean;
    route: string;
    onRoad: boolean;
    area: string;
    clockOut?: {time: Date | number; location: {lat: number; long: number}; };
}
