export interface Route {
    route: string;
    area: string;
    van: string;
    rabbit: string;
    packages?: Array<Package>;
    lWarehouse?: {time: Date | number; location: {lat: number; long: number}; };
    rtsArrival?: {time: Date | number; location: {lat: number; long: number}; };
    rtsDeparture?: {time: Date | number; location: {lat: number; long: number}; };
    // tslint:disable-next-line: max-line-length
    da: { name: string; id: string; clockIn: {time: Date | number; location: {lat: number; long: number}; }; clockOut: {time: Date | number; location: {lat: number; long: number}; }; };
    fStop?: {time: Date | number; location: {lat: number; long: number}; };
    fAttempt?: {time: Date | number; location: {lat: number; long: number}; };
    lAttempt?: {time: Date | number; location: {lat: number; long: number}; };
    rtsScan?: {time: Date | number; location: {lat: number; long: number}; };
}

export interface Package {
    route: string;
    actions: [{date: number; markedBy: string; markedAs: string; reason?: string; to?: string; }];
    tbc: string;
    rts?: boolean;
    extra?: {pickedUp: boolean; assigned: boolean; };
    area?: string;
}
