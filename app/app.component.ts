import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { RtsService } from './rts.service';
import { first } from 'rxjs/operators';
import { AppUpdate } from '@ionic-native/app-update/ngx';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  user: any;
  interval;
  subs;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private service: RtsService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
    });
  }

  sendVerificationEmail() {
    this.service.sendEmailVerification().then(() => {
      this.service.presentToast('success', `email sent ! please restart your app after verifying.`);
    });
  }

  ngOnInit(): void {
    this.service.presentLoadingWithOptions();
    this.service.getStorageItem('showSlide').then(val => {
      if (val === 'true') {
        // this.showSlides = false;
      }
   });

    this.subs = this.service.getCurrentUser().subscribe(user => {
    this.service.dismissLoading();
    this.splashScreen.hide();
    return this.user = user;
   });
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
