import { TestBed } from '@angular/core/testing';

import { RtsService } from './rts.service';

describe('RtsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RtsService = TestBed.get(RtsService);
    expect(service).toBeTruthy();
  });
});
