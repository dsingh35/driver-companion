import { Component, OnInit, OnDestroy, ChangeDetectorRef, AfterViewChecked, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { RtsService } from '../rts.service';
import { AlertController, IonSlides } from '@ionic/angular';
import { Package, Route } from 'src/pkg.modal';
import { Subscription, Observable } from 'rxjs';
import { DaDay } from 'src/daday.modal';
import { ModalController } from '@ionic/angular';
import { ContactComponent } from '../contact/contact.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})

export class HomePage implements OnInit, OnDestroy, AfterViewChecked {
  @ViewChild('slides', {static: false}) slides: IonSlides;
  clockIn: FormGroup;
  fd: FormGroup;
  panelOpenState = false;
  daDay;
  fStop = false;
  start = false;
  fTry = false;
  lTry = false;
  rtsScan = false;
  arrived = false;
  uta: Array<Package> = [];
  fdd: Array<Package> = [];
  missing: Array<Package> = [];
  extra: Array<Package> = [];
  bc: Array<Package> = [];
  rej: Array<Package> = [];
  utl: Array<Package> = [];
  oodt: Array<Package> = [];
  dl: Array<Package> = [];
  assigned: Array<Package> = [];
  route;
  offlineRoute;
  scanSlide = {slidesPerView: 1.1, spaceBetween: 5};
  linear = false;
  rts: Array<Package> = [];
  stepperIndex = 0;
  user;
  clockedOut = false;
  sliderOptions = {watchSlidesProgress: true, allowSlidePrev: false, allowSlideNext: false};
  subscription: Array<Subscription> = [];
  today = new Date(Date.now());
  // tslint:disable-next-line: max-line-length
  constructor(private fb: FormBuilder, private service: RtsService, private alertController: AlertController, private cdRef: ChangeDetectorRef,
              private modalController: ModalController) {
    this.service.presentLoadingWithOptions('getting your day');
    this.subscription.push(this.service.getOffline$().subscribe(r => {
      this.route = r;
      if (this.route && this.route.da) {
        this.linear = false;
        this.stepperIndex = 0;
        this.route.da.clockIn  && (this.start = true) && (this.stepperIndex = 1);
        this.linear = false;
        this.route.da.clockIn && !this.route.lWarehouse && this.service.startBackgroundGeolocation();
        this.linear = false;
        this.route.fStop && (this.stepperIndex=2) && (this.fStop = true);
        this.linear = false;
        this.route.lWarehouse && !this.route.lAttempt && this.service.stopTracking();
        this.linear = false;
        this.route.fAttempt && (this.stepperIndex = 4) && (this.fTry = true);
        this.linear = false;
        this.route.lAttempt && (this.stepperIndex=5) && (this.lTry = true);
        this.linear = false;
        this.route.lAttempt && !this.route.rtsDeparture && this.service.startBackgroundGeolocation();
        this.linear = false;
        this.route.rtsArrival && (this.arrived = true) && this.nextSlide(1);
        this.linear = false;
        this.route.rtsScan && (this.stepperIndex=6) && (this.rtsScan = true);
        this.linear = false;
        this.route.rtsDeparture && this.service.stopTracking();
        this.linear = false;
        this.route.da.clockOut && (this.clockedOut = true);
        this.linear = false;
        this.cdRef.detectChanges();
        this.separatePkgs(r.packages);
      }
      setTimeout(() => {
        this.linear = true;
      }, 1000);
    }));
    setTimeout(() => {
      this.service.dismissLoading();
    }, 4000);
    this.subscription.push(this.service.getUser$().subscribe(user => {
      this.user = user;
    }));
    this.subscription.push(this.service.getDaDay().subscribe(d => {
      this.daDay = d;
    }));
    this.subscription.push(this.service.getRoute().subscribe(r => {
      if (r) {
        this.route = r;
        this.offlineRoute = this.route;
        this.service.setStorageItem('route', JSON.stringify(this.route)).then(() => {
          this.service.setOfflineRoute(this.route);
        });
      }
      this.service.dismissLoading();
    }));
  }

  setRoute(route) {
    this.route = route;
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ContactComponent
    });
    return await modal.present();
  }

  separatePkgs(pkgs) {
    this.uta = []; this.utl = []; this.fdd = []; this.bc = []; this.dl = []; this.extra = []; this.missing = []; this.oodt = [];
    this.rej = []; this.assigned = [];
    // tslint:disable-next-line: forin
    for ( const tbc in pkgs) {
      switch ( pkgs[tbc].actions[pkgs[tbc].actions.length - 1].markedAs.trim()) {
        case 'UTA':
          this.uta.push(pkgs[tbc]);
          break;
        case 'UTL':
          this.utl.push(pkgs[tbc]);
          break;
        case 'FDD':
          this.fdd.push(pkgs[tbc]);
          break;
        case 'BC':
          this.bc.push(pkgs[tbc]);
          break;
        case 'REJECTED':
          this.rej.push(pkgs[tbc]);
          break;
        case 'OODT':
          this.oodt.push(pkgs[tbc]);
          break;
        case 'DELIVERED':
          this.dl.push(pkgs[tbc]);
          break;
          case 'MANUAL':
            this.dl.push(pkgs[tbc]);
            break;
        case 'EXTRA':
          this.extra.push(pkgs[tbc]);
          break;
        case 'MISSING':
          this.missing.push(pkgs[tbc]);
          break;
        case 'ASSIGNED':
          this.assigned.push(pkgs[tbc]);
          break;
      }
    }
    console.log(this.assigned);
  }

  onBreak() {
    if (!this.route && !this.route.breaks) {
      return false;
    }
    for (const key in this.route.breaks) {
      if (this.route.breaks.hasOwnProperty(key)) {
        const t = this.getDate(this.route.breaks[key].time);
        if (key === '30') {
          t.setMinutes(t.getMinutes() + 30);
        } else {
          t.setMinutes(t.getMinutes() + 15);
        }
        if (t >= new Date()) {
         return true;
       }
      }
    }
    return false;
  }

  ngOnInit() {
  }

  puchBreak(bType) {
    if (this.onBreak()) {
      this.service.presentToast('', 'you are already on a break');
      return;
    }
    this.service.punchBreak(bType);
  }
  rtsPkgMark(pkg) {
    this.service.presentRTSActionSheet(pkg);
  }

  markRTSPkgs(tbc, prob: string) {
    let temp = -1;
    switch (prob.trim()) {
      case 'UTA':
        temp = this.uta.findIndex(p => p.tbc === tbc);
        if (temp !== -1) {
          this.uta[temp].rts = true;
          this.uta[temp].actions.push({markedAs: 'RTS', markedBy: this.user.name, date: Date.now()});
          return true;
        }
        return false;
      case 'UTL':
        temp = this.utl.findIndex(p => p.tbc === tbc);
        if (temp !== -1) {
          this.utl[temp].rts = true;
          this.utl[temp].actions.push({markedAs: 'RTS', markedBy: this.user.name, date: Date.now()});
          return true;
        }
        return false;
      case 'FDD':
        temp = this.fdd.findIndex(p => p.tbc === tbc);
        if (temp !== -1) {
          this.fdd[temp].rts = true;
          this.fdd[temp].actions.push({markedAs: 'RTS', markedBy: this.user.name, date: Date.now()});
          return true;
        }
        return false;
      case 'BC':
        temp = this.bc.findIndex(p => p.tbc === tbc);
        if (temp !== -1) {
          this.bc[temp].rts = true;
          this.bc[temp].actions.push({markedAs: 'RTS', markedBy: this.user.name, date: Date.now()});
          return true;
        }
        return false;
      case 'REJECTED':
        temp = this.rej.findIndex(p => p.tbc === tbc);
        if (temp !== -1) {
          this.rej[temp].rts = true;
          this.rej[temp].actions.push({markedAs: 'RTS', markedBy: this.user.name, date: Date.now()});
          return true;
        }
        return false;
      case 'OODT':
        temp = this.oodt.findIndex(p => p.tbc === tbc);
        if (temp !== -1) {
          this.oodt[temp].rts = true;
          this.oodt[temp].actions.push({markedAs: 'RTS', markedBy: this.user.name, date: Date.now()});
          return true;
        }
        return false;
      case 'EXTRA':
        temp = this.extra.findIndex(p => p.tbc === tbc);
        if (temp !== -1) {
          this.extra[temp].rts = true;
          this.extra[temp].actions.push({markedAs: 'RTS', markedBy: this.user.name, date: Date.now()});
          return true;
        }
        return false;
      case 'ASSIGNED':
          temp = this.assigned.findIndex(p => p.tbc === tbc);
          if (temp !== -1) {
            this.assigned[temp].rts = true;
            this.assigned[temp].actions.push({markedAs: 'RTS', markedBy: this.user.name, date: Date.now()});
            return true;
          }
          return false;
      case 'MISSING':
        temp = this.missing.findIndex(p => p.tbc === tbc);
        if (temp !== -1) {
          this.missing[temp].rts = true;
          this.missing[temp].actions.push({markedAs: 'RTS', markedBy: this.user.name, date: Date.now()});
          return true;
        }
        return false;
    }
  }

  arrivedAtStation() {
    this.service.markRTSArrival();
  }

  finishSlide(prob) {
    switch (prob.trim()) {
      case 'UTA':
        this.service.markRTSPkg(this.uta);
        break;
      case 'UTL':
        this.service.markRTSPkg(this.utl);
        break;
      case 'FDD':
        this.service.markRTSPkg(this.fdd);
        break;
      case 'BC':
        this.service.markRTSPkg(this.bc);
        break;
      case 'REJECTED':
        this.service.markRTSPkg(this.rej);
        break;
      case 'OODT':
        this.service.markRTSPkg(this.oodt);
        break;
      case 'DELIVERED':
        this.service.markRTSPkg(this.dl);
        break;
      case 'EXTRA':
        this.service.markRTSPkg(this.extra);
        break;
      case 'ASSIGNED':
        this.service.markRTSPkg(this.assigned);
        break;
      case 'MISSING':
        this.service.markRTSPkg(this.missing);
        break;
    }
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  clockOut() {
    this.service.clockOut({route: this.route.route, area: this.route.area});
  }

  userClockIn(serviceArea, routeNumber, van, rabbit) {
    const route = '' + this.service.getConfigByArea(serviceArea).routeSignature.split(',')[0].trim().toUpperCase() + routeNumber;
    this.service.clockIn(this.user.name, {route, area: serviceArea, van, rabbit});
  }

  punchFD() {
    this.service.punchFD(this.route.da.name,  {route: this.route.route, area: this.route.area});
  }

  lAttempt() {
    this.service.punchlAttempt();
  }

  fAttempt() {
    this.service.punchfAttempt(this.route.da.name,  {route: this.daDay.route, area: this.daDay.area});
  }

  needToScan(prob) {
    switch (prob) {
      case 'UTA':
        return this.uta.filter(pkg => !pkg.rts);
      case 'UTL':
        return this.utl.filter(pkg => !pkg.rts);
      case 'FDD':
        return this.fdd.filter(pkg => !pkg.rts);
      case 'BC':
        return this.bc.filter(pkg => !pkg.rts);
      case 'REJECTED':
        return this.rej.filter(pkg => !pkg.rts);
      case 'OODT':
        return this.oodt.filter(pkg => !pkg.rts);
      case 'ASSIGNED':
        return this.assigned.filter(pkg => !pkg.rts);
      case 'EXTRA':
        return this.extra.filter(pkg => !pkg.rts);
    }
    return [];
  }

  getDate(date) {
    if (typeof date.toDate === 'function') {
      return date.toDate();
    }

    if (date.seconds) {
      return new Date(Number(date.seconds) * 1000);
    }

    return date;
  }

  nextSlide(index?) {
    this.slides.isEnd().then((result) => {
      if (result) {
        this.service.finishRTSScan({route: this.daDay.route, area: this.daDay.area});
      } else {
        this.slides.lockSwipeToNext(false).then(() => {
          if (index) {
              this.slides.slideTo(index);
          } else {
            this.slides.slideNext();
          }
          this.slides.lockSwipeToNext(true);
        });
      }
    });
  }

  async getTBC() {
    const alert = await this.alertController.create({
      header: 'Enter TBC',
      animated: true,
      inputs: [
        {
          name: 'TBC',
          type: 'text',
          placeholder: 'TBC...'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'Submit',
          handler: (val) => {
            if (this.service.isValidTBC(val.TBC)) {
              const pkg: Package = {
                tbc: val.TBC.trim(),
                actions: [{markedBy: this.user.name, markedAs: '', date: Date.now()}],
                route: this.route.route,
                area: this.route.area
              };
              this.service.presentActionSheet(pkg);
            } else {
              this.service.presentToast('danger', 'incorrect TBC');
            }
          }
        }
      ]
    });

    await alert.present();
  }

  print(q) {
    console.log(q);
  }

  async issues() {
    let scannedTBCs = '';
    const alert = await this.alertController.create({
      header: 'Issues',
      mode: 'ios',
      message: 'Please select the issue you are facing',
      buttons: [
        {
          text: 'Rabbit Issues',
          role: 'confirm',
          cssClass: 'primary ion-text-capitalize ion-text-center',
          handler: async () => {
            const rabbit_t = await this.alertController.create({
            header: 'TroubleShoot',
            mode: 'ios',
            message: '<small>Asking dispatch about it is always a good idea.</small>',
            inputs: [
              {
                type: 'text',
                placeholder: 'Deliverd to ?',
              },
              {
                type: 'text',
                placeholder: 'TBC numbers separated by a \',\'',
                value: scannedTBCs
              }
            ],
            buttons: [
              {
                text: 'Scan',
                role: 'confirm',
                handler: () => {
                  this.service.scanBarcode().then((value) => {
                    if (value.text && this.service.isValidTBC(value.text)) {
                      scannedTBCs += value.text.trim().toUpperCase() + ',';
                      rabbit_t.present();
                    } else {
                      this.service.presentToast('danger', 'wrong barcode');
                    }
                  });
                }
              },
              {
                text: 'Submit',
                role: 'submit',
                handler: () => {
                  console.log('submit');
                }
              },
              {
                text: 'Cancel',
                role: 'cancel',
                handler: () => {
                  console.log('cacelled');
                }
              }
            ]
           });
            await rabbit_t.present();
          }
        },
        {
          text: 'Incomplete group delivery',
          role: 'confirm',
          cssClass: 'primary ion-text-capitalize ion-text-center',
          handler: () => {
           console.log('group');
          }
        },
        {
          text: 'Alternate address request',
          role: 'confirm',
          cssClass: 'primary ion-text-capitalize ion-text-center',
          handler: () => {
           console.log('alternate address');
          }
        }
      ]
    });
    await alert.present();
  }

  actionOnPkg(pkg: Package) {
    pkg.actions.push({date: Date.now(), markedBy: this.user.name, markedAs: ''});
    this.service.presentActionSheet(pkg);
  }

  scanRTSPkg(prob) {
    this.service.scanBarcode().then((result) => {
      if (result.text && this.service.isValidTBC(result.text)) {
        if (this.markRTSPkgs(result.text, prob)) {

        } else {
          this.service.presentToast('warn', 'wrong barcode scanned');
        }
      } else {
          document.getElementsByClassName('tbcScan')[0].classList.add('tada');
          this.service.presentToast('warn', 'incorrect barcode');
      }
    }).catch((err) => {
      this.service.presentToast('warn', 'unable to scan');
      this.getTBC();
    });
  }

  scanTBC(options) {
    this.service.scanBarcode().then((result) => {
      if (result.cancelled && !options || !options.search) {
        this.getTBC();
      } else {
        this.service.presentToast('success', result.text);
      }
    }).catch((err) => {
      if (!options || !options.search) {
        this.getTBC();
      }
    });
  }

  markNewPackage() {
    this.service.scanBarcode().then((result) => {
      if (result.text.length < 1 || result.cancelled) {
        this.getTBC();
      } else if (this.service.isValidTBC(result.text)) {
        const pkg: Package = {
          tbc: result.text,
          actions: [{markedBy: this.user.name, markedAs: '', date: Date.now()}],
          route: this.route.route,
          area: this.route.area
        };
        this.service.presentActionSheet(pkg);
      } else {
        this.service.presentToast('warn', 'incorrect barcode');
      }
    }).catch((err) => {
      this.getTBC();
    });
  }

  ngOnDestroy(): void {
    this.subscription.forEach(subs => {
      subs.unsubscribe();
    });
  }
}
