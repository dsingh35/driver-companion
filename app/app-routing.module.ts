import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'performance', loadChildren: './performance/performance.module#PerformancePageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { pathMatch: 'full', redirectTo: 'home', path: '**' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
