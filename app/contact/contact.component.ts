import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {
  manager = {name: 'Chris Wenger', phone: '778-111-1111'};
  dispatch = {name: 'Dispatch', phone: '112-222-3333'};
  service = {name: 'Ford Road Side', phone: '222-333-3333'};
  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {}

  dismiss() {
    this.modalCtrl.dismiss();
  }

}
