import { Component, OnInit, ChangeDetectorRef, AfterViewChecked } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AlertController } from '@ionic/angular';
import { RtsService } from '../rts.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, AfterViewChecked {

  userForm: FormGroup;
  LogInForm: FormGroup;
  newUser = true; // to toggle login or signup form
  passReset = false;

  formErrors = {
    email: '',
    password: ''
  };

  validationMessages = {
    email: {
      required:      'Email is required.',
      email:         'Email must be a valid email'
    },
    password: {
      required:      'Password is required.',
      pattern:       'Password must be include at one letter and one number.',
      minlength:     'Password must be at least 4 characters long.',
      maxlength:     'Password cannot be more than 40 characters long.',
    }
  };

  // tslint:disable-next-line: max-line-length
  constructor(private fb: FormBuilder, private service: RtsService, private alertController: AlertController, private cdRef: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.buildSignUpForm();
    this.buildLogInForm();
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  buildSignUpForm(): void {
    this.userForm = this.fb.group({
      email: new FormControl('', [
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ]
      ),
      password: new FormControl('', Validators.compose([
        Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9!@\$]+)$'),
        Validators.minLength(6),
        Validators.maxLength(25)
        ])
      ),
    name: new FormControl('', Validators.compose([
      Validators.minLength(3),
      Validators.maxLength(20)
      ])
    ),
    tel: new FormControl('', Validators.compose([
      Validators.required,
      Validators.minLength(10),
      Validators.maxLength(10)
      ])
    ),
    cPassword: new FormControl('', Validators.compose([
      Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9!@\$]+)$'),
      Validators.minLength(6),
      Validators.maxLength(25)
    ]))
  }, {validator: this.checkPasswords});

    this.userForm.reset();
    this.userForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // reset validation messages
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
  const pass = group.controls.password.value;
  const confirmPass = group.controls.cPassword.value;

  return pass === confirmPass ? null : { notSame: true };
  }

  onValueChanged(_data?: any) {
    if (!this.userForm) { return; }
    const form = this.userForm;
    // tslint:disable-next-line: forin
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        // tslint:disable-next-line: forin
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  buildLogInForm() {
    this.LogInForm = this.fb.group({
      email: new FormControl('', [
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ]
      ),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9!@\$]+)$'),
        Validators.minLength(6),
        Validators.maxLength(25)
        ])
      )
  });
  }

  logIn() {
    if (this.LogInForm.valid) {
      this.service.presentLoadingWithOptions();
      this.service.emailLogin(this.LogInForm).then(() => {
        this.service.switchComponent('list');
      }).catch(() => {
        this.service.presentToast('danger', 'Please check your email and password !');
      }).finally(() => {
        this.service.dismissLoading();
      });
    }
  }

  async presentAlertPrompt() {
    const alert = await this.alertController.create({
      header: 'Enter your  email',
      inputs: [
        {
          name: 'email',
          type: 'email',
          placeholder: 'account@something.com'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Send',
          handler: (val) => {
            console.log(val);
            this.resetPassword(val.email);
          }
        }
      ]
    });

    await alert.present();
  }

  resetPassword(email?: string) {
    if (/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/.test(email)) {
      this.service.presentLoadingWithOptions('sending');
      // tslint:disable-next-line: max-line-length
      this.service.sendPasswordResetEmail(email).then(() =>  this.service.presentToast('success', 'email sent! check your inbox'))
      .catch(() => this.service.presentToast('danger', 'Error! user may not be registered'))
      .finally(() => this.service.dismissLoading());
    }
  }

  sendVerificationEmail() {
    this.service.sendEmailVerification().then(() => {
      this.service.presentToast('success', `email has been sent to ${this.service.getSyncUser().email}`);
    });
  }

  signUp() {
    if (this.userForm.valid) {
      this.service.presentLoadingWithOptions();
      this.service.emailSignUp(this.userForm).then(() => {
        this.sendVerificationEmail();
      }).catch( (error: Error) => {
        this.service.presentToast('warn', error.message);
      }).finally(() => {
        this.service.dismissLoading();
      });
    }
  }

}
