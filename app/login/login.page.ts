import { Component, OnInit, OnDestroy } from '@angular/core';
import { RtsService } from '../rts.service';
import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit, OnDestroy {
  user;
  subscription: Subscription;
  edit = false;
  constructor(private service: RtsService) {
    this.user = this.service.getUser();
    if (!this.user) {
      this.subscription = this.service.getUser$().subscribe((user) => {
        this.user = user;
      });
    }
   }

   signOut() {
     if (this.edit) {
       this.edit = false;
     } else {
       this.service.signOut();
     }
   }

   editUser() {
    if (!this.edit) {
       this.edit = true;
       return;
     }
    const u = { phone : ( document.getElementById('phone') as HTMLInputElement).value,
     pEmail: ( document.getElementById('pEmail') as HTMLInputElement).value};

    if (!/^[0-9]{10}$/.test(u.phone)) {
      document.getElementById('phone').classList.add('error');
      this.service.presentToast('', 'number must be 10 characters long');
     } else {
      document.getElementById('phone').classList.remove('error');
     }

    if (!/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/.test(u.pEmail)) {
      document.getElementById('pEmail').classList.add('error');
      return;
    } else {
      document.getElementById('pEmail').classList.remove('error');
    }

    const mUser = u;
    for (const key in u) {
      if (u.hasOwnProperty(key)) {
        if (u[key] === this.user[key]) {
          delete mUser[key];
        }
      }
    }
    if (Object.keys(mUser).length === 0) {
      // this.service.editUser(u);
    }
   }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
  }

}
