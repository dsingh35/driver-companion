import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { IonicModule } from '@ionic/angular';
import { ChartsModule } from 'ng2-charts';
import { MatExpansionModule } from '@angular/material/expansion';
import { PerformancePage } from './performance.page';

const routes: Routes = [
  {
    path: '',
    component: PerformancePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    MatExpansionModule,
    FormsModule,
    ChartsModule,
    NgCircleProgressModule.forRoot(),
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PerformancePage]
})
export class PerformancePageModule {}
