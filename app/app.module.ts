import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AngularFireAuthGuard, emailVerified } from '@angular/fire/auth-guard';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { LoginComponent } from './login/login.component';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { MatTabsModule } from '@angular/material/tabs';
import { MatStepperModule } from '@angular/material/stepper';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation/ngx';
import { AppRoutingModule } from './app-routing.module';
import { AngularFireFunctionsModule } from '@angular/fire/functions';
import { AppComponent } from './app.component';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { MatInputModule } from '@angular/material/input';
import { ContactComponent } from './contact/contact.component';
import { Network } from '@ionic-native/network/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { GlobalErrorHandler } from './GlobalErrorHandler';
import { AppUpdate } from '@ionic-native/app-update/ngx';

@NgModule({
  declarations: [AppComponent, LoginComponent, ContactComponent],
  entryComponents: [ContactComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatInputModule,
    IonicModule.forRoot(),
    AngularFireModule.initializeApp({
      apiKey: 'AIzaSyDfITP0rpzwcfdws4qwXe_bmoTBPts7X8M',
      authDomain: 'rtsbclt.firebaseapp.com',
      databaseURL: 'https://rtsbclt.firebaseio.com',
      projectId: 'rtsbclt',
      storageBucket: 'rtsbclt.appspot.com',
      messagingSenderId: '563482083080',
      appId: '1:563482083080:web:9124586d8c096eb7aa36bd'
    },
    AngularFirestoreModule.enablePersistence()),
    AppRoutingModule,
    AngularFireModule,
    FormsModule,
    AngularFireAuthModule,
    MatStepperModule,
    AngularFireFunctionsModule,
    IonicStorageModule.forRoot(),
    AngularFirestoreModule,
    ReactiveFormsModule,
  ],
  providers: [
    AngularFireAuthGuard,
    Geolocation,
    Network,
    {provide: ErrorHandler, useClass: GlobalErrorHandler},
    BackgroundMode,
    LocalNotifications,
    BackgroundGeolocation,
    StatusBar,
    AppUpdate,
    BarcodeScanner,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
