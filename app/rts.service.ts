import { Injectable, OnDestroy } from '@angular/core';
import { AngularFirestore , AngularFirestoreCollection, AngularFirestoreDocument} from '@angular/fire/firestore';
import { Observable } from 'rxjs/Observable';
import { switchMap, take, first, share, catchError } from 'rxjs/operators';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observer, of, Subscriber, observable, Subscription, config } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { LoadingController } from '@ionic/angular';
import * as geolib from 'geolib';
import { ToastController } from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { Storage } from '@ionic/storage';
// tslint:disable-next-line: max-line-length
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationEvents, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Package, Route } from '../pkg.modal';
import { DaDay } from '../daday.modal';
import { AngularFireFunctions } from '@angular/fire/functions';
import { AlertController } from '@ionic/angular';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { Network } from '@ionic-native/network/ngx';
import { AppUpdate } from '@ionic-native/app-update/ngx';

@Injectable({
  providedIn: 'root'
})

export class RtsService implements OnDestroy {
  user$: Observable<any>;
  userLocation: any;
  wareHouseCoords = {lat: 49.246410, lng: -122.947220};
  daDay$: Observable<DaDay>;
  daDay;
  uploadingPending = false;
  configSubs: Subscription;
  configurations = [];
  subscription = [];
  networkSub = [];
  offlineObserver;
  offline$: Observable<any>;
  public offlineRoute: any = {};
  connected = true;
  user;
  route$: Observable<Route>;
  geoTrackTemp = {lWarehouse: false, arrived: false, departed: false};
  today: string;
  constructor(private toastController: ToastController, private afAuth: AngularFireAuth, private afs: AngularFirestore,
              private router: Router, public loadingController: LoadingController, public actionSheetController: ActionSheetController,
              private storage: Storage, private geolocation: Geolocation, private barcodeScanner: BarcodeScanner,
              private fns: AngularFireFunctions, private alertController: AlertController,
              private backgroundGeolocation: BackgroundGeolocation, private localNotifications: LocalNotifications, private network: Network
            , private backgroundMode: BackgroundMode, private appUpdate: AppUpdate) {
    const date = new Date(Date.now());
    this.today = `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`;
    this.afs.doc('tech/app').valueChanges().pipe(first()).toPromise().then((doc: any) => {
      console.log(doc);
      if (!doc) {
        return;
      }
      this.appUpdate.checkAppUpdate(doc.update.trim()).then(update => {
        console.log(update);
      }).catch(error => {
        console.log(error);
      });
    });

    if (!this.offlineObserver) {
      this.offline$ = new Observable(observer => {
        this.offlineObserver = observer;
        this.getStorageItem('route').then(r => {
          try {
            this.offlineRoute = JSON.parse(r);
          } catch (error) {
            this.offlineRoute = r;
          }
          this.offlineRoute = this.offlineRoute || {};
          this.offlineObserver.next(this.offlineRoute);
        });
      });
    }
    this.networkSub.push(this.network.onConnect().subscribe(() => {
      this.connected = true;
      this.getStorageItem('pending').then(reqs => {
        if (JSON.parse(reqs) && JSON.parse(reqs).length > 0 && !this.uploadingPending) {
          this.uploadingPending = true;
          this.postPending(JSON.parse(reqs));
        }
      });
    }));

    this.networkSub.push(this.network.onDisconnect().subscribe(() => {
      this.connected = false;
    }));

    this.getConfigurations();
    this.configBackgroundGeolocation();
    this.user$ = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<any>(`drivers/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      }),
      catchError(e => of(null))
    );
    this.daDay$ = this.user$.pipe(
      switchMap(user => {
        if (user) {
          this.user = user;
          return this.afs.doc(`drivers/${user.id}`).collection(`shifts`).doc<DaDay>(`${this.today}`).valueChanges();
        } else {
        return of(null);
        }
      }),
      catchError(e => of(null))
    );
    this.route$ = this.daDay$.pipe(
      switchMap( day => {
        if (day) {
          this.daDay = day;
          return this.afs.doc(`routes/${this.today}`).collection(day.area).doc<Route>(day.route).valueChanges();
        } else {
          return of(null);
        }
      }),
      catchError(e => of(null))
    );
  }

  setOfflineRoute(route) {
    if (this.offlineObserver) {
      this.offlineObserver.next(route);
    }
  }

  getOffline$() {
    return this.offline$;
  }

  isOnline() {
    return this.connected;
  }

  stopTracking() {
    this.backgroundGeolocation.stop().then(() => this.configBackgroundGeolocation());
    this.backgroundMode.disable();
    this.localNotifications.cancelAll();
  }

  getConfigByArea(area) {
    return this.configurations.find(config => config.area.trim().toUpperCase() === area.trim().toUpperCase());
  }

  async markWarehoueseDeparture(location, time?) {
    console.log('in func', location);
    this.geoTrackTemp.lWarehouse = true;
    const coords = {lat: location.latitude, lng: location.longitude};
    if (!this.connected) {
        this.getStorageItem('pending').then((value) => {
          let array = [];
          try {
            array = JSON.parse(value);
          } catch (error) {
            console.log(error);
          }
          console.log(value, array);
          if (value && array && array.length > 0) {
            // tslint:disable-next-line: max-line-length
            array.push({name: 'RTS', action: 'lWarehouse', area: this.offlineRoute.area, coords, route: this.offlineRoute.route, time: time || Date.now()});
          } else {
            // tslint:disable-next-line: max-line-length
            array = [{name: 'RTS', action: 'lWarehouse', area: this.offlineRoute.area, coords, route: this.offlineRoute.route, time: time || Date.now()}];
          }
          this.setStorageItem('pending', JSON.stringify(array));
          this.presentToast('', 'request is saved offline');
          if (this.offlineRoute && !this.offlineRoute.lWarehouse) {
            this.offlineRoute.lWarehouse = {time: time || Date.now(), coords};
            this.setStorageItem('route', this.offlineRoute);
            this.offlineObserver.next(this.offlineRoute);
            this.backgroundGeolocation.deleteAllLocations();
          }
        });
      } else {
        console.log(time);
        const callable = this.fns.httpsCallable('RTS');
        // tslint:disable-next-line: max-line-length
        callable({time, route: this.offlineRoute.route, action: 'lWarehouse', area: this.offlineRoute.area, coords}).pipe(first()).toPromise().then((result) => {
          if (result.Error) {
            this.presentToast('danger', result.Error);
          } else {
            this.presentToast('success', 'You have left the station');
            this.offlineRoute.lWarehouse  = {time: time || Date.now(), coords};
            this.backgroundGeolocation.deleteAllLocations();
            this.stopTracking();
          }
        }).catch((error) => {
          return Promise.reject(error);
        });
      }
  }

  unsubscribe() {
    for (const sub of this.subscription) {
      sub.unsubscribe();
    }
    this.subscription = [];
  }

  async markRTSDeparture(location, time?) {
    this.geoTrackTemp.departed = true;
    const coords = {lat: location.latitude, lng: location.longitude};
    if (!this.connected) {
      this.getStorageItem('pending').then((value) => {
        let array = [];
        try {
          array = JSON.parse(value);
        } catch (error) {
          console.log(error);
        }
        if (value && array && array.length > 0) {
          // tslint:disable-next-line: max-line-length
          array.push({name: 'RTS', action: 'RTSDeparture', area: this.offlineRoute.area, coords, route: this.offlineRoute.route, time: Date.now()});
        } else {
          // tslint:disable-next-line: max-line-length
          array = [{name: 'RTS', action: 'RTSDeparture', area: this.offlineRoute.area, coords, route: this.offlineRoute.route, time: Date.now()}];
        }
        this.setStorageItem('pending', JSON.stringify(array));
        this.stopTracking();
      });
    }
    const callable = this.fns.httpsCallable('RTS');
    // tslint:disable-next-line: max-line-length
    callable({time, route: this.offlineRoute.route, action: 'rtsDeparture', area: this.offlineRoute.area, coords}).pipe(first()).toPromise().then((result) => {
      if (result.Error) {
        this.presentToast('danger', result.Error);
      } else {
        this.presentToast('success', 'Your day has been completed');
        this.stopTracking();
      }
    }).catch((error) => {
      this.presentToast('danger', 'Please try again');
      this.geoTrackTemp.departed = false;
      return Promise.reject(error);
    });
  }

  async markRTSArrival(auto?: boolean, time?) {
    await this.backgroundGeolocation.getCurrentLocation({enableHighAccuracy: true, maximumAge: 60000, timeout: 30000}).then((location) => {
      const coords = {lat: location.latitude, lng: location.longitude};
      // tslint:disable-next-line: max-line-length
      if (auto || !this.getConfigByArea(this.offlineRoute.area).geoFences.warehouse.inforced || geolib.isPointWithinRadius(coords, this.getConfigByArea(this.offlineRoute.area).geoFences.warehouse.coords , this.getConfigByArea(this.offlineRoute.area).geoFences.warehouse.radius)) {
        this.presentLoadingWithOptions('marking arrival');
        this.geoTrackTemp.arrived = true;
        if (!this.connected) {
          this.getStorageItem('pending').then((value) => {
            let array = [];
            try {
              array = JSON.parse(value);
            } catch (error) {
              console.log(error);
            }
            if (value && array && array.length > 0) {
              // tslint:disable-next-line: max-line-length
              array.push({name: 'RTS', action: 'rtsArrival', time: time || Date.now(), area: this.offlineRoute.area, coords, route: this.offlineRoute.route});
            } else {
             // tslint:disable-next-line: max-line-length
             array = [{name: 'RTS', action: 'rtsArrival', time: time || Date.now(), area: this.offlineRoute.area, coords, route: this.offlineRoute.route}];
            }
            this.setStorageItem('pending', JSON.stringify(array));
            if (this.offlineRoute && !this.offlineRoute.rtsArrival) {
              this.offlineRoute.rtsArrival = {time: time || Date.now(), coords};
              this.setStorageItem('route', this.offlineRoute);
              this.backgroundGeolocation.deleteAllLocations();
              this.offlineObserver.next(this.offlineRoute);
            }
          });
        } else {
          const callable = this.fns.httpsCallable('RTS');
          // tslint:disable-next-line: max-line-length
          callable({time, route: this.offlineRoute.route, action: 'rtsArrival', area: this.offlineRoute.area, coords}).pipe(first()).toPromise().then((result) => {
            if (result.Error) {
              this.presentToast('danger', result.Error);
            } else {
              this.presentToast('success', 'You have arrived at the station');
              this.offlineRoute.rtsArrival = {time: time || Date.now(), coords};
              this.backgroundGeolocation.deleteAllLocations();
            }
          }).catch((error) => {
            this.presentToast('danger', 'Please try again');
            this.geoTrackTemp.arrived = false;
          });
          }
        } else {
            this.presentToast('danger', 'you are not at the station');
          }
        }).catch((err) => {
          console.log(err);
          this.presentToast('danger', 'unable to get your location');
        });
    }

  scanBarcode() {
    return this.barcodeScanner.scan({showTorchButton : true, resultDisplayDuration: 0});
  }

  postPending(req) {
    if (this.connected) {
      this.presentLoadingWithOptions('posting offline data');
      const callable = this.fns.httpsCallable('pending');
      this.uploadingPending = true;
      callable(req).pipe(first()).toPromise().then((result) => {
        if (result.Error) {
          this.presentToast('danger', result.Error);
          this.uploadingPending = false;
          this.setStorageItem('route', JSON.stringify({}));
        }
        this.presentToast('', 'data uploaded');
        this.setStorageItem('pending', JSON.stringify([]));
      }).catch((err) => {
        console.log(err);
        this.uploadingPending = true;
        this.presentToast('', 'Please try again');
      });
    } else {
      this.presentToast('', 'you are not connected');
    }
  }

  signOut() {
    this.setStorageItem('route', JSON.stringify({}));
    this.setStorageItem('pending', JSON.stringify([]));
    this.afAuth.auth.signOut().then(() => {
      this.presentToast('success', 'you have signed out');
    }).catch(() => {
      this.presentToast('danger', 'Something went wrong');
    });
  }

  editUser(u) {
    this.presentLoadingWithOptions('updating profile');
    if (!this.connected) {
      this.getStorageItem('pending').then((value) => {
        let array = [];
        if (value && array && array.length > 0) {
          array = JSON.parse(value);
          array.push({name: 'updateUser', data: u});
        } else {
          array = [{name: 'updateUser', data: u}];
        }
        this.setStorageItem('pending', JSON.stringify(array));
      });
    }
    const callable = this.fns.httpsCallable('updateUser');
    callable(u).pipe(first()).toPromise().then((result) => {
      if (result.Error) {
        this.presentToast('danger', result.Error);
      } else {
        this.presentToast('success', 'profile has been updated');
      }
    }).catch((err) => {
      this.presentToast('danger', 'Something went wrong');
    });
  }

getDaDay() {
    return this.daDay$;
  }

getRoute() {
    return this.route$;
  }

configBackgroundGeolocation() {
  const c: BackgroundGeolocationConfig = {
      desiredAccuracy: 10,
      stationaryRadius: 20,
      distanceFilter: 50,
      debug: false,
      stopOnTerminate: false,
      interval: 10000,
      fastestInterval: 5000,
      activitiesInterval: 10000,
    };

  this.subscription.length <= 0 && this.backgroundGeolocation.configure(c).then(() => {
    // tslint:disable-next-line: no-unused-expression
    console.log('configuring', this.getConfigByArea('burnaby'));
    this.subscription.push(this.backgroundGeolocation
        .on(BackgroundGeolocationEvents.location)
        .subscribe((location: BackgroundGeolocationResponse) => {
           console.log('executing', this.getConfigByArea(this.offlineRoute.area), this.offlineRoute);
           const coords = {lat: location.latitude, lng:  location.longitude};
           this.getStorageItem('configs').then((c) => {
             try {
              this.configurations = JSON.parse(c);
             } catch (err) {
               console.log(err);
             }
             this.backgroundGeolocation.getLocations().then((locations) => {
               console.log(locations);
               for (const l of locations) {
                console.log(new Date(l.time).toLocaleTimeString(), this.offlineRoute, this.configurations);
                const coord = {lat: l.latitude, lng: l.longitude};
                // tslint:disable-next-line: max-line-length
                if (this.configurations && this.offlineRoute && !this.offlineRoute.lWarehouse && !this.offlineRoute.fStop && this.offlineRoute.da && this.offlineRoute.da.clockIn && !geolib.isPointWithinRadius(coord, this.getConfigByArea(this.offlineRoute.area).geoFences.warehouse.coords, this.getConfigByArea(this.offlineRoute.area).geoFences.warehouse.radius)) {
                  console.log('marking lWarehouse');
                  this.offlineRoute.lWarehouse = {time: new Date(l.time), location: coord};
                  this.markWarehoueseDeparture(l, l.time);
                  break;
                  // tslint:disable-next-line: max-line-length
                } else if (this.configurations && this.offlineRoute && this.offlineRoute.lAttempt && !this.offlineRoute.rtsArrival && geolib.isPointWithinRadius(coord, this.getConfigByArea(this.offlineRoute.area).geoFences.warehouse.coords, this.getConfigByArea(this.offlineRoute.area).geoFences.warehouse.radius)) {
                  console.log('marjking rtsA');
                  this.offlineRoute.rtsArrival = {time: new Date(l.time), location: coord};
                  this.markRTSArrival(true, l.time);
                  break;
                  // tslint:disable-next-line: max-line-length
                } else if (this.configurations && this.offlineRoute && this.offlineRoute.da && !this.offlineRoute.da.clockOut && this.offlineRoute.rtsArrival && !this.offlineRoute.rtsDeparture && !geolib.isPointWithinRadius(coord, this.getConfigByArea(this.offlineRoute.area).geoFences.warehouse.coords, this.getConfigByArea(this.offlineRoute.area).geoFences.warehouse.radius)) {
                  if (!this.offlineRoute.rtsScan) {
                  } else {
                    console.log('marking depart');
                    this.offlineRoute.rtsDeparture = {time: new Date(l.time), location: coord};
                    this.markRTSDeparture(l, l.time);
                    break;
                  }
                }
              }
            // tslint:disable-next-line: max-line-length
               if (!this.offlineRoute.lWarehouse && !this.offlineRoute.fStop && this.offlineRoute.da.clockIn && !geolib.isPointWithinRadius(location, this.getConfigByArea(this.offlineRoute.area).geoFences.warehouse.coords, this.getConfigByArea(this.offlineRoute.area).geoFences.warehouse.radius)) {
              console.log('marking lWarehouse');
              this.offlineRoute.lWarehouse = {time: new Date(location.time), location: coords};
              this.markWarehoueseDeparture(location);
            // tslint:disable-next-line: max-line-length
            } else if (this.offlineRoute.lAttempt && !this.offlineRoute.rtsArrival && geolib.isPointWithinRadius(location, this.getConfigByArea(this.offlineRoute.area).geoFences.warehouse.coords, this.getConfigByArea(this.offlineRoute.area).geoFences.warehouse.radius)) {
              console.log('marjking rtsA');
              this.offlineRoute.rtsArrival = {time: new Date(location.time), location: coords};
              this.markRTSArrival(true);
            // tslint:disable-next-line: max-line-length
            } else if (!this.offlineRoute.da.clockOut && this.offlineRoute.rtsArrival && !this.offlineRoute.rtsDeparture && !geolib.isPointWithinRadius(location, this.getConfigByArea(this.offlineRoute.area).geoFences.warehouse.coords, this.getConfigByArea(this.offlineRoute.area).geoFences.warehouse.radius)) {
              if (!this.offlineRoute.rtsScan) {
                this.presentToast('danger', 'finish your RTS process first');
              } else {
              console.log('marking depart');
              this.offlineRoute.rtsDeparture = {time: new Date(location.time), location: coords};
              this.markRTSDeparture(location);
              }
            }
               console.log('end');
          });
        });
        }));
      });
  this.backgroundGeolocation.finish();

  this.backgroundGeolocation.isLocationEnabled().then((result) => {
      if (result === 0) {
        setTimeout(() => {
          const showSettings = confirm('App requires location tracking permission. Would you like to open app settings?');
          if (showSettings) {
            return this.backgroundGeolocation.showAppSettings();
          }
        }, 1000);
      }
    }).catch((err) => {

    });

    // start recording location
  }

  startBackgroundGeolocation() {
    // this.backgroundGeolocation.watchLocationMode().subscribe((status) => {
    //   console.log(status);
    // });
      this.backgroundMode.enable();
      this.backgroundMode.disableWebViewOptimizations();
      this.localNotifications.schedule({
        id: 1,
        text: 'GPS running'
      });
      this.backgroundGeolocation.start();
  }

  async getUserLocation() {
    return this.backgroundGeolocation.getCurrentLocation({enableHighAccuracy: true, maximumAge: 60000, timeout: 30000});
  }

  clockIn(daName, route) {
    this.presentLoadingWithOptions('clocking in');
    this.backgroundGeolocation.getCurrentLocation({enableHighAccuracy: true, maximumAge: 60000, timeout: 30000}).then((result) => {
      console.log(this.getConfigByArea(route.area));
      const coords = {lat: result.latitude, lng: result.longitude};
      // tslint:disable-next-line: max-line-length
      if ( this.getConfigByArea(route.area).geoFences.warehouse.inforced && !geolib.isPointWithinRadius(coords, this.getConfigByArea(route.area).geoFences.warehouse.coords , this.getConfigByArea(route.area).geoFences.warehouse.radius)) {
        return this.presentToast('danger', 'you are not at the station');
      }
      if (false) {
        this.getStorageItem('pending').then((value) => {
          let array = [];
          try {
            array = JSON.parse(value);
          } catch (error) {
            console.log(error);
          }

          if (value && array) {
            array.push({name: 'clockIn', daName, route: route.route, van: route.van, rabbit: route.rabbit, area: route.area, coords});
          } else {
            array = [{name: 'clockIn', daName, route: route.route, van: route.van, rabbit: route.rabbit, area: route.area, coords}];
          }
          if (!this.offlineRoute) {
            this.offlineRoute = {};
          }
          if (!this.offlineRoute.da) {
            this.offlineRoute = {route: route.route, van: route.van, rabbit: route.rabbit, area: route.area};
            this.offlineRoute.da = {name: daName, clockIn: {time: Date.now(), location: coords}};
            this.setStorageItem('route', JSON.stringify(this.offlineRoute)).then(() => {
              this.offlineObserver.next(this.offlineRoute);
            });
          }
          this.setStorageItem('pending', JSON.stringify(array));
          this.backgroundGeolocation.deleteAllLocations();
          this.startBackgroundGeolocation();
          this.presentToast('', 'request is saved offline');
        });
      } else {
        const callable = this.fns.httpsCallable('clockIn');
        // tslint:disable-next-line: max-line-length
        callable({daName, route: route.route, van: route.van, rabbit: route.rabbit, area: route.area, coords}).pipe(first()).toPromise().then((result) => {
          if (result.Error) {
            this.presentToast('danger', result.Error);
          } else {
            if (!this.offlineRoute || !this.offlineRoute.da) {
              this.offlineRoute = {route: route.route, van: route.van, rabbit: route.rabbit, area: route.area};
              this.offlineRoute.da = {name: daName, clockIn: {time: Date.now(), location: coords}};
            }
            this.backgroundGeolocation.deleteAllLocations();
            this.startBackgroundGeolocation();
            this.presentToast('success', 'clocked in');
          }
        }).catch((err) => {
          this.presentToast('danger', 'Something went wrong, Please try again!');
        });
      }
    }).catch((err) => {
      console.log(err);
      this.presentToast('danger', 'unable to get your location');
    });
  }

  punchBreak(bType) {
    this.presentLoadingWithOptions('punching break');
    this.backgroundGeolocation.getCurrentLocation({enableHighAccuracy: true, maximumAge: 60000, timeout: 30000}).then((result) => {
      const coords = {lat: result.latitude, lng: result.longitude};
      console.log(coords);
      if (!this.connected) {
        this.getStorageItem('pending').then((value) => {
          let array = [];
          try {
            array = JSON.parse(value);
          } catch (error) {
            console.log(error);
          }

          if (value && array) {
            array.push({name: 'punchBreak', bType, time: Date.now(), area: this.offlineRoute.area, coords, route: this.offlineRoute.route});
          } else {
            array = [{name: 'punchBreak', bType, time: Date.now(), area: this.offlineRoute.area, coords, route: this.offlineRoute.route}];
          }
          if (!this.offlineRoute.breaks) {
            this.offlineRoute.breaks = {};
          }
          if (!this.offlineRoute.breaks[bType.trim()]) {
            this.offlineRoute[bType] = {bType, time: Date.now()};
            this.setStorageItem('route', this.offlineRoute);
            this.offlineObserver.next(this.offlineRoute);
          }
          this.setStorageItem('pending', JSON.stringify(array));
          this.presentToast('', 'request is saved offline');
        });
      } else {
        const callable = this.fns.httpsCallable('punchBreak');
        console.log(bType, this.daDay.route, this.daDay.area);
        callable({bType, route: this.daDay.route, area: this.daDay.area, coords}).pipe(first()).toPromise().then((result) => {
          if (result.Error) {
          this.presentToast('danger', result.Error);
        } else {
          this.presentToast('success', `${bType} puched in`);
        }
      }).catch((_err) => {
        console.log(_err);
        this.presentToast('danger', 'Something went wrong, Please try again!');
      });
    }
  }).catch((err) => {
      console.log(err);
      this.presentToast('danger', 'unable to get your location');
    });
  }

  getConfigurations() {
    if (!this.connected) {
      this.getStorageItem('configs').then(c => this.configurations = JSON.parse(c));
    } else {
      this.configSubs = this.afs.collection('configurations').valueChanges().pipe(
      catchError(e => of(null))
      ).subscribe(configs => {
        this.configurations = configs;
        this.setStorageItem('configs', JSON.stringify(this.configurations));
      });
    }
  }

  punchFD(daName?, route?) {
    this.presentLoadingWithOptions('marking first stop');
    this.backgroundGeolocation.getCurrentLocation({enableHighAccuracy: true, maximumAge: 60000, timeout: 30000}).then((result) => {
      const coords = {lat: result.latitude, lng: result.longitude};
      if (!this.connected) {
        this.getStorageItem('pending').then((value) => {
          let array = [];
          try {
            array = JSON.parse(value);
          } catch (error) {
            console.log(error);
          }

          if (value && array) {
            array.push({name: 'punchFD', area: this.offlineRoute.area, coords, route: this.offlineRoute.route, time: Date.now()});
          } else {
            array = [{name: 'punchFD', area: this.offlineRoute.area, coords, route: this.offlineRoute.route, time: Date.now()}];
          }
          if (!this.offlineRoute.fStop) {
            this.offlineRoute.fStop = {time: Date.now(), coords};
            this.setStorageItem('route', JSON.stringify(this.offlineRoute));
            this.offlineObserver.next(this.offlineRoute);
          }
          this.setStorageItem('pending', JSON.stringify(array));
          this.offlineObserver.next(this.offlineRoute);
          this.backgroundGeolocation.deleteAllLocations();
          this.presentToast('', 'request is saved offline');
        });
      } else {
        console.log(this.offlineRoute);
        const callable = this.fns.httpsCallable('punchFD');
        // tslint:disable-next-line: max-line-length
        callable({route: this.offlineRoute.route, area: this.offlineRoute.area, coords}).pipe(first()).toPromise().then((result) => {
          if (result.Error) {
            this.presentToast('danger', result.Error);
          } else {
            this.presentToast('success', 'FD puched in');
            this.backgroundGeolocation.deleteAllLocations();
          }
        }).catch((_err) => {
          this.presentToast('danger', 'Something went wrong, Please try again!');
        });
      }
    }).catch((err) => {
      console.log(err);
      this.presentToast('danger', 'unable to get your location');
    });
  }

  punchfAttempt(daName, route) {
    this.presentLoadingWithOptions('marking first attempt');
    this.backgroundGeolocation.getCurrentLocation({enableHighAccuracy: true, maximumAge: 60000, timeout: 30000}).then((result) => {
      const coords = {lat: result.latitude, lng: result.longitude};
      if (!this.connected) {
        this.getStorageItem('pending').then((value) => {
          let array = [];
          try {
            array = JSON.parse(value);
          } catch (error) {
            console.log(error);
          }

          if (value && array && array.length > 0) {
            array.push({name: 'fAttempt', time: Date.now(), area: this.daDay.area, coords, route: this.daDay.route});
          } else {
            array = [{name: 'fAttempt', time: Date.now(), area: this.daDay.area, coords, route: this.daDay.route}];
          }
          if (!this.offlineRoute.fAttempt) {
            this.offlineRoute.fAttempt = {time: Date.now(), coords};
            this.setStorageItem('route', JSON.stringify(this.offlineRoute));
            this.offlineObserver.next(this.offlineRoute);
          }
          this.setStorageItem('pending', JSON.stringify(array));
          this.presentToast('', 'request is saved offline');
        });
      } else {
        const callable = this.fns.httpsCallable('punchfAttempt');
        callable({route: route.route, area: route.area, coords}).pipe(first()).toPromise().then((result) => {
          if (result.Error) {
            this.presentToast('danger', result.Error);
          } else {
            this.presentToast('success', 'first attempt done');
          }
        }).catch((_err) => {
          this.presentToast('danger', 'Something went wrong, Please try again!');
        });
      }
    }).catch((err) => {
      console.log(err);
      this.presentToast('danger', 'unable to get your location');
    });
  }

  punchlAttempt() {
    this.presentLoadingWithOptions('marking last attempt');
    this.backgroundGeolocation.getCurrentLocation({enableHighAccuracy: true, maximumAge: 60000, timeout: 30000}).then((result) => {
      const coords = {lat: result.latitude, lng: result.longitude};
      console.log(coords);
      if (!this.connected) {
        this.getStorageItem('pending').then((value) => {
          let array = [];
          try {
            array = JSON.parse(value);
          } catch (err) {
            this.presentToast('', 'please try again');
          }
          if (value && array && array.length > 0) {
            array.push({name: 'lAttempt', time: Date.now(), area: this.offlineRoute.area, coords, route: this.offlineRoute.route});
          } else {
            array = [{name: 'lAttempt', time: Date.now(), area: this.offlineRoute.area, coords, route: this.offlineRoute.route}];
          }
          if (!this.offlineRoute.lAttempt) {
            this.offlineRoute.lAttempt = {time: Date.now(), coords};
            this.setStorageItem('route', JSON.stringify(this.offlineRoute));
            this.offlineObserver.next(this.offlineRoute);
          }
          this.setStorageItem('pending', JSON.stringify(array));
          this.presentToast('', 'request is saved offline');
        });
      } else {
        const callable = this.fns.httpsCallable('punchlAttempt');
        // tslint:disable-next-line: max-line-length
        callable({route: this.offlineRoute.route, area: this.offlineRoute.area, coords}).pipe(first()).toPromise().then((result) => {
          if (result.Error) {
            this.presentToast('danger', result.Error);
          } else {
          this.presentToast('success', 'last attempt done');
          this.offlineRoute.lAttempt = {time: Date.now(), coords};
          this.startBackgroundGeolocation();
          }
        }).catch((_err) => {
          this.presentToast('danger', 'Something went wrong, Please try again!');
        });
      }
    }).catch((err) => {
      console.log(err);
      this.presentToast('danger', 'unable to get your location');
    });
  }

  clockOut(route, rts?) {
    this.presentLoadingWithOptions('clocking out');
    this.backgroundGeolocation.getCurrentLocation({enableHighAccuracy: true, maximumAge: 60000, timeout: 30000}).then((result) => {
      const coords = {lat: result.latitude, lng: result.longitude};
      // tslint:disable-next-line: max-line-length
      if ( this.getConfigByArea(route.area).geoFences.warehouse.inforced && !geolib.isPointWithinRadius(coords, this.getConfigByArea(route.area).geoFences.warehouse.coords , this.getConfigByArea(route.area).geoFences.warehouse.radius)) {
        return this.presentToast('danger', 'you are not at the station');
      }
      if (false) {
        this.getStorageItem('pending').then((value) => {
          let array = [];
          try {
            array = JSON.parse(value);
          } catch (error) {
            console.log(error);
          }

          if (value && array) {
            array.push({name: 'clockOut', route: route.route, area: route.area, coords, rts: false});
          } else {
            array = [{name: 'clockOut', route: route.route, area: route.area, coords, rts: false}];
          }
          if (!this.offlineRoute.clockOut) {
            this.offlineRoute.clockOut = {time: Date.now(), coords};
            this.setStorageItem('route', JSON.stringify(this.offlineRoute));
            this.offlineObserver.next(this.offlineRoute);
          }
          this.setStorageItem('pending', JSON.stringify(array));
          this.presentToast('', 'request is saved offline');
          this.stopTracking();
        });
      } else {
        const callable = this.fns.httpsCallable('clockOut');
        const data = {route: route.route, area: route.area, coords, rts: false};
        if (rts) {
          data.rts = true;
        }
        callable(data).pipe(first()).toPromise().then((result) => {
          if (result.Error) {
            this.presentToast('danger', result.Error);
          } else {
            this.presentToast('success', 'clocked out');
            this.stopTracking();
          }
        }).catch((_err) => {
          // this.presentToast('danger', 'Something went wrong, Please try again!');
        });
      }
    }).catch((err) => {
      console.log(err);
      this.presentToast('danger', 'unable to get your location');
    });
  }

  async getStorageItem(item: string) {
    let value: string;
    if (item.trim() === 'pending') {
      this.uploadingPending = false;
    }
    await this.storage.ready().then(async () => {
      await this.storage.get(item).then(val => value = val);
    });
    return value;
  }

  setStorageItem(key, value) {
    console.log(key, value);
    return this.storage.ready().then(() => {
      this.storage.set(key, value);
    });
  }

  finishRTSScan(route) {
    this.presentLoadingWithOptions('finishing RTS');
    this.backgroundGeolocation.getCurrentLocation({enableHighAccuracy: true, maximumAge: 60000, timeout: 30000}).then((result) => {
      const coords = {lat: result.latitude, lng: result.longitude};
      // tslint:disable-next-line: max-line-length
      if ( this.getConfigByArea(this.daDay.area).geoFences.warehouse.inforced && !geolib.isPointWithinRadius(coords, this.getConfigByArea(this.daDay.area).geoFences.warehouse.coords , this.getConfigByArea(this.daDay.area).geoFences.warehouse.radius)) {
        return this.presentToast('danger', 'you are not at the station');
      }
      if (!this.connected) {
        this.getStorageItem('pending').then((value) => {
          let array = [];
          try {
            array = JSON.parse(value);
          } catch (error) {
            console.log(error);
          }

          if (value && array) {
            array.push({name: 'rtsScan', route: route.route, action: 'rtsScan', area: route.area, coords});
           } else {
            array = [{name: 'rtsScan', route: route.route, action: 'rtsScan', area: route.area, coords}];
          }
          this.setStorageItem('pending', JSON.stringify(array));
          this.presentToast('', 'request is saved offline');
        });
      }
      const callable = this.fns.httpsCallable('RTS');
      callable({route: route.route, action: 'rtsScan', area: route.area, coords}).pipe(first()).toPromise().then((result) => {
        if (result.Error) {
          this.presentToast('danger', result.Error);
        } else {
          this.presentToast('success', 'Your RTS process is complete');
        }
      }).catch((_err) => {
        this.presentToast('danger', 'Please try again');
        });
    }).catch((_err) => {
      this.presentToast('danger', 'failed to get your location');
    });
  }

  markRTSPkg(pkgs: Package[]) {
    this.presentLoadingWithOptions('marking');
    if (false) {
      this.getStorageItem('pending').then((value) => {
        let array = [];
        if (value && array && array.length > 0) {
          const array = JSON.parse(value);
          array.push({name: 'markRTSPkgs', pkgs, area: pkgs[0].area, route: pkgs[0].route});
        } else {
          array = [{name: 'markRTSPkgs', pkgs, area: pkgs[0].area, route: pkgs[0].route}];
        }
        this.setStorageItem('pending', JSON.stringify(array));
      });
    }
    const callable = this.fns.httpsCallable('markRTSPkgs');
    callable({pkgs, area: pkgs[0].area, route: pkgs[0].route}).pipe(first()).toPromise().then((result) => {
       if (result.Error) {
          this.presentToast('danger', result.Error);
          } else {
      }
        }).catch((_err) => {
      this.presentToast('danger', 'failed to mark pkgs RTS.');
    });
  }

  markPackage(pkg: Package, action?, reason?, to?) {
    if (action) {
      pkg.actions[pkg.actions.length - 1].markedAs = action;
      if (action.trim().toUpperCase() === 'MANUAL') {
        pkg.actions[pkg.actions.length - 1].reason = reason;
        pkg.actions[pkg.actions.length - 1].to = to;
      }
    }
    this.presentLoadingWithOptions('marking');
    if (!this.connected) {
      this.getStorageItem('pending').then((value) => {
        let array = [];
        try {
          array = JSON.parse(value);
        } catch (error) {
          console.log(error);
        }
        if (value && array) {
          const i = array.findIndex(a => a.name.trim() === 'markPackage');
          if (i !== -1) {
            const index = array[i].packages.findIndex((p) => p.tbc.trim() === pkg.tbc.trim());
            if (index !== -1) {
              array[i].packages[index] = pkg;
            } else {
              array[i].packages.push(pkg);
            }
          } else {
            array.push({name: 'markPackage',  packages: [pkg], area: this.offlineRoute.area, route: this.offlineRoute.route});
          }
        } else {
          array = [{name: 'markPackage', packages: [pkg], area: this.offlineRoute.area, route: this.offlineRoute.route}];
        }
        this.setStorageItem('pending', JSON.stringify(array));
        if (!this.offlineRoute.packages) {
          this.offlineRoute.packages =  {};
        }
        this.offlineRoute.packages[pkg.tbc.trim()] =  pkg;
        this.setStorageItem('route', JSON.stringify(this.offlineRoute)).then(() => {
          this.offlineObserver.next(this.offlineRoute);
        });
        this.presentToast('', 'package marked offline');
      });
    } else {
      const callable = this.fns.httpsCallable('markPackage');
      callable(pkg).pipe(first()).toPromise().then((result) => {
        if (result.Error) {
            this.presentToast('danger', result.Error);
          } else {
          this.presentToast('success', `packages have been marked as ${action}`);
        }
      }).catch((_err) => {
        this.presentToast('danger', 'failed to mark anything. Try again !');
      });
    }
  }

  async presentRTSActionSheet(pkg: Package) {
    const actionSheet = await this.actionSheetController.create({
      header: pkg.tbc,
      translucent: true,
      buttons: [
      {
        text: 'MISSING',
        handler: () => {
          this.markPackage(pkg, 'MISSING');
        }
      }, {
        text: 'DELIVERED',
        handler: () => {
          this.markPackage(pkg, 'DELIVERED');
        }
      }, {
        text: 'Cancel',
        role: 'destructive',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  markRTSPkgs(_pkgs: Package[]) {

  }

  isValidTBC(tbc: string) {
    if (tbc && tbc.trim().length === 15 && tbc.startsWith('TBC', 0)) {
      return true;
    }
    return false;
  }

  atWarehouse() {
    this.backgroundGeolocation.getCurrentLocation({enableHighAccuracy: true, maximumAge: 60000, timeout: 30000}).then((result) => {
      const coords = {lat: result.latitude, lng: result.longitude};
      // tslint:disable-next-line: max-line-length
      if ( this.getConfigByArea(this.daDay.area).geoFences.warehouse.inforced && !geolib.isPointWithinRadius(coords, this.getConfigByArea(this.daDay.area).geoFences.warehouse.coords , this.getConfigByArea(this.daDay.area).geoFences.warehouse.radius)) {
        this.presentToast('danger', 'you are not at the station');
        return false;
      }
      return true;
    });
  }

  async presentActionSheet(pkg: Package) {
    const actionSheet = await this.actionSheetController.create({
      header: pkg.tbc,
      translucent: true,
      buttons: [{
        text: 'UTA',
        handler: () => {
          this.markPackage(pkg, 'UTA');
        }
      },  {
        text: 'UTL',
        handler: async () => {
          const alert = await this.alertController.create({
            header: 'Have you tried calling your Dispatch?',
            subHeader: pkg.tbc,
            message: 'Please always call dispatch before marking package as UTL.',
            buttons: [
              {
                text: 'Yes',
                role: 'confirm',
                cssClass: 'primary',
                handler: () => {
                  pkg.extra = {pickedUp: true, assigned: true};
                  this.markPackage(pkg, 'UTL');
                }
              },
              {
                text: 'No',
                role: 'cancel',
                cssClass: 'secondary',
                handler: () => {
                  console.log('Cancel');
                }
              }
            ]
          });
          await alert.present();
        }
      }, {
        text: 'FDD',
        handler: () => {
          this.markPackage(pkg, 'FDD');
        }
      },
      {
        text: 'BC',
        handler: () => {
          this.markPackage(pkg, 'BC');
        }
      }, {
        text: 'EXTRA',
        handler: async () => {
          const alert = await this.alertController.create({
            header: 'Extra package picked up?',
            subHeader: pkg.tbc,
            message: 'Please try signing out and back in from your rabbit before doing anything.',
            buttons: [
              {
                text: 'Yes',
                role: 'confirm',
                cssClass: 'primary',
                handler: () => {
                  pkg.extra = {pickedUp: true, assigned: true};
                  this.markPackage(pkg, 'EXTRA');
                }
              },
              {
                text: 'No',
                role: 'confirm',
                cssClass: 'primary',
                handler: () => {
                  pkg.extra = {pickedUp: false, assigned: false};
                  this.markPackage(pkg, 'EXTRA');
                }
              },
              {
                text: 'Cancel',
                role: 'cancel',
                cssClass: 'secondary',
                handler: () => {
                  console.log('Cancel');
                }
              }
            ]
          });
          await alert.present();
        }
      }, {
        text: 'MISSING',
        handler: () => {
          this.markPackage(pkg, 'MISSING');
        }
      }, {
        text: 'OODT',
        handler: () => {
          this.markPackage(pkg, 'OODT');
        }
      }, {
        text: 'REJ/DMG',
        handler: () => {
          this.markPackage(pkg, 'REJECTED');
        }
      }, {
        text: 'DELIVERED',
        handler: () => {
          this.markPackage(pkg, 'DELIVERED');
        }
      },
      {
        text: 'DELIVERED MANUALLY',
        handler: async () => {
          const present = await this.alertController.create({
            header: 'Issues',
            mode: 'ios',
            message: 'Please select the reason of a manual delivery',
            inputs: [
              {
                name: 'reason',
                type: 'text',
                placeholder: 'Reason for the manual delivery'
              },
              {
                name: 'to',
                type: 'text',
                placeholder: 'Delivered to? eg. cx or front door etc.'
              }
            ],
            buttons: [
              {
                text: 'Submit',
                role: 'confirm',
                handler: (data) => {
                  console.log(data);
                  if (!data || !data.reason) {
                    return this.presentToast('danger', 'Please enter a reason');
                  }

                  if (!data || !data.to) {
                    return this.presentToast('danger', 'Please enter how did you deliver');
                  }
                  this.markPackage(pkg, 'MANUAL', data.reason, data.to);
                }
              },
              {
                text: 'Cancel',
                role: 'destructive',
                handler: () => {
                  console.log('Cancel clicked');
                }
              }
            ]
          });
          await present.present();
        }
      },
      {
        text: 'Cancel',
        role: 'destructive',
        handler: () => {
          console.log('Cancel clicked');
        }
      }
    ]
    });
    await actionSheet.present();
  }

  async presentToast(color, message) {
    try {
      this.loadingController.getTop().then((result) => {
        if (result) {
          this.dismissLoading();
        }
      }).catch((err) => {
        console.log(err);
      });
      const toast = await this.toastController.create({
      message,
      duration: 2000
    });
      toast.present();
    } catch (error) {
      console.log(error);
    }
  }

  switchComponent(name: string) {
    this.router.navigate([name]);
  }

  getSyncUser() {
    return this.afAuth.auth.currentUser;
  }

  async presentLoadingWithOptions(message?: string) {
    const loading = await this.loadingController.create({
      duration: 0,
      mode: 'ios',
      translucent: true,
      message: message || '',
      spinner: 'dots',
    });
    await loading.present();
    return loading;
  }

  dismissLoading() {
    try {
      this.loadingController.dismiss();
    } catch (error) {
      console.log(error);
    }
  }

  emailSignUp(formData) {
    const userData = {
      name: formData.get('name').value,
      phoneNumber: formData.get('tel').value,
      email: formData.get('email').value,
      password: formData.get('password').value,
      uid: ''
    };
    return this.afAuth.auth.createUserWithEmailAndPassword(userData.email, userData.password)
      .then(user => {
        userData.uid = user.user.uid;
        userData.password = '';
        return this.setUserDoc(userData);
      })
      .catch(error => this.handleError(error) );
  }

  async emailLogin(formData) {
    const credentials: {email: string, password: string} = {
      email: formData.get('email').value,
      password: formData.get('password').value
    };
    return await this.afAuth.auth.signInWithEmailAndPassword(credentials.email, credentials.password);
  }

  async sendEmailVerification() {
    await this.afAuth.auth.currentUser.sendEmailVerification();
  }

  getCurrentUser() {
    return this.afAuth.authState;
  }

  updateUser(user: any, data: any) {
    return this.afs.doc(`users/${user.uid}`).update(data);
  }

  getUser$() {
    return this.user$;
  }

  getUser() {
    return this.user;
  }

  async refreshUser() {
    this.user$ = await this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<any>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      }),
      catchError(e => of(null))
    );
    return this.user$;
  }

  async sendPasswordResetEmail(passwordResetEmail: string) {
    return await this.afAuth.auth.sendPasswordResetEmail(passwordResetEmail);
 }

  private handleError(error) {
    console.error(error);
  }

  private setUserDoc(userData) {
    if (!this.connected) {
      this.getStorageItem('pending').then((value) => {
        let array = [];
        try {
          array = JSON.parse(value);
        } catch (error) {
          console.log(error);
        }
        if (value && array && array.length > 0) {
          array.push({name: 'createUser', data: userData});
        } else {
          array = [{name: 'createUser', data: userData}];
        }
        this.setStorageItem('pending', JSON.stringify(array));
      });
    }
    const callable = this.fns.httpsCallable('createUser');
    callable(userData);
  }

  ngOnDestroy() {
    this.configSubs.unsubscribe();
    this.networkSub.forEach(sub => {
      sub.unsubscribe();
    });
  }

}
